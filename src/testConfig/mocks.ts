import { IAppContext } from '../components/AppContext';

export interface IContextWrapper {
    context?: IAppContext;
}

export const initialContext: IAppContext = {
    repos: {
        listState: {
            list: [],
            isLoading: false,
        },
        searchPhrase: '',
        liked: [],
        onSearch: jest.fn(),
        likeRepo: jest.fn()
    },
    languages: {
        list: [],
        selected: '',
        onSelect: jest.fn()
    },
    isError: false
};