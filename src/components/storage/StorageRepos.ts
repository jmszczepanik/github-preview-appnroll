
export const fetchLikedRepos = (fn: Function) => {
    const repos = localStorage.getItem('likedRepos');

    if(!repos) {
      return;
    }
  
    try {
      const liked = JSON.parse(repos);
      fn(liked)
    } catch (e) {
      throw new Error('Wrong format of repositories in local storage');
    }
  }
  
export const likeRepoById = (repoList: number[], fn: Function) => (id: number) => {
    const likedRepos = [...repoList];
    let newRepos;
    if(!~likedRepos.indexOf(id)) {
        newRepos = [...likedRepos, id];
    } else {
        newRepos = likedRepos.filter(repo => repo !== id);
    }

    localStorage.setItem('likedRepos', JSON.stringify(newRepos));
    fn(newRepos);
}
  