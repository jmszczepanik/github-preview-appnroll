import { fetchLikedRepos, likeRepoById } from "../StorageRepos"

describe('Local storage test', () => {

    afterEach(() => {
        localStorage.clear();
    })

    it('should not perform action if store is empty', () => {
        const fn = jest.fn();

        fetchLikedRepos(fn);

        expect(fn).not.toHaveBeenCalled();
    })

    it('should call action if store not empty', () => {
        const fn = jest.fn();

        localStorage.setItem('likedRepos', JSON.stringify([1,2,3]));
        fetchLikedRepos(fn);

        expect(fn).toHaveBeenCalled();
    })

    it('should throw an error', () => {
        localStorage.setItem('likedRepos', "{foo: bar}");

        expect(fetchLikedRepos).toThrow();
    })

    it('should add liked repo', () => {
        const fn = jest.fn();
        const liked = [1,2,3];
        const newId = 4;
        const like = likeRepoById(liked, fn);

        like(newId);

        expect(fn).toBeCalledWith([...liked, newId]);
    })

    it('should remove liked repo if already exists', () => {
        const fn = jest.fn();
        const liked = [1,2,3];
        const newId = 3;
        const like = likeRepoById(liked, fn);

        like(newId);

        expect(fn).toBeCalledWith(liked.filter(id => id !== newId));
    })
})