import React from 'react';
import { IRepositoryListState } from './repository/RepositoryType';

export const initialRepoState = {
    list: [],
    isLoading: true
  };

export interface IAppContext {
    languages: {
        list: string[];
        selected: string;
        onSelect: React.Dispatch<React.SetStateAction<string>>;
    };
    repos: {
        listState: IRepositoryListState;
        searchPhrase: string;
        liked: number[];
        onSearch: React.Dispatch<React.SetStateAction<string>>;
        likeRepo: (id: number) => void;
    },
    isError: boolean;
}

export default React.createContext<IAppContext>({
    repos: {
        listState: initialRepoState,
        searchPhrase: '',
        liked: [],
        onSearch: () => {
            throw new Error('onSearch not implemented');
        },
        likeRepo: () => {
            throw new Error('likeRepo not implemented');
        }
    },
    languages: {
        list: [],
        selected: '',
        onSelect: () => {
            throw new Error('onSelect not implemented');
        }
    },
    isError: false
});