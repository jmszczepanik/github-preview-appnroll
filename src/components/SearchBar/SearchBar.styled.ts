import styled from 'styled-components';
import { Button } from '../styles/CommonStyles.styled';

export const SearchBarContainer = styled.div`
    display: flex;
    margin-top: 45px;
`;

export const SelectWrapper = styled.div`
    margin-left: 16px;
`;

export const SearchBarButton = styled(Button)`
    margin-left: 16px;
`;