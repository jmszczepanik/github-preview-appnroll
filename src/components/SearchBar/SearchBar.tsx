import React, { ChangeEvent, MouseEvent } from 'react';
import AppContext from '../AppContext';
import Select from '../common/select/SelectInput';
import { SearchBarContainer, SelectWrapper, SearchBarButton } from './SearchBar.styled';
import { TextInput } from '../styles/CommonStyles.styled';


const SearchBar: React.FC = () => {
    const {languages, repos} = React.useContext(AppContext);

    const handleSearch =
        (e: ChangeEvent<HTMLInputElement>) => repos.onSearch(e.target.value);

    const handleSelect =
        (option: string) => languages.onSelect(option);

    const clear = (e: MouseEvent) => {
        e.preventDefault();
        repos.onSearch('');
        languages.onSelect('');
    }

    return (
        <SearchBarContainer>
            <TextInput
                type="text"
                name="search"
                onChange={handleSearch}
                value={repos.searchPhrase}
                placeholder={'Search'}
            />
            <SelectWrapper>
                <Select
                    value={languages.selected}
                    onChange={handleSelect}
                    defaultValue={'Select language'}
                    options={languages.list}
                />
            </SelectWrapper>
            <SearchBarButton onClick={clear}>Clear filters</SearchBarButton>
        </SearchBarContainer>
    )
}

export default SearchBar;