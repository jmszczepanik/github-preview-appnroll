import React from 'react';
import { IGithubRepository } from './RepositoryType';
import { Tile, LanguageColorHeader, RepositoryContent, RepositoryHeader, RepositoryName, RepositoryButton, RepositoryLink, RepositoryFooter, RepositoryLanguage, RepositoryPropertyList, TileContent } from './Repository.styled';
import Add from '../../assets/add.svg';
import Added from '../../assets/added.svg';
import External from '../../assets/external.svg';
import Stars from '../../assets/stars.svg';
import Visitors from '../../assets/visitors.svg';
import Issues from '../../assets/issues.svg';
import Language from '../../assets/language.svg';
import { RepositoryPropertyElement } from '../repository/Repository.styled';
import colors from '../../assets/colors.json';

interface IRepositoryProps {
    repo: IGithubRepository;
    isLiked: boolean;
    onLike: (id: number) => void;
}

interface IRepositoryProperty {
    type: RepositoryPropertyType;
    value: number;
    image: string;
}

enum RepositoryPropertyType {
    STARS = 'stargazers_count',
    VISITORS = 'watchers_count',
    ISSUES = 'forks_count'
}

interface IGithubColors {
    [key: string]: string;
}

const propertyImages = {
    [RepositoryPropertyType.ISSUES]: Issues,
    [RepositoryPropertyType.VISITORS]: Visitors,
    [RepositoryPropertyType.STARS]: Stars,
};

const getProperties = (repo: IGithubRepository): IRepositoryProperty[] => {
    const props = Object.values(RepositoryPropertyType);

    return props.map(type => ({
        type,
        value: repo[type],
        image: propertyImages[type]
    }))
}

const Repository: React.FC<IRepositoryProps> = ({repo, isLiked, onLike}) => {
    const handleClick = () => onLike(repo.id);
    const ButtonImage = isLiked ? Added : Add;
    
    const properties = getProperties(repo);
    const color = (colors as IGithubColors)[repo.language];

    return (
        <Tile>
            <LanguageColorHeader color={color}/>
            <TileContent>
                <RepositoryContent>
                    <RepositoryHeader>
                        <RepositoryName>
                            {repo.name}
                        </RepositoryName>
                        <RepositoryButton onClick={handleClick}>
                            <ButtonImage/>
                        </RepositoryButton>
                    </RepositoryHeader>
                    <RepositoryLink href={repo.html_url} target="_blank" >
                        <External/>
                        <span>
                            {repo.full_name}
                        </span>
                    </RepositoryLink>
                </RepositoryContent>
                <RepositoryFooter>
                    <RepositoryPropertyElement color={color}>
                        <Language/>
                        <RepositoryLanguage>
                            {repo.language}
                        </RepositoryLanguage>
                    </RepositoryPropertyElement>
                    <RepositoryPropertyList>
                        {properties.map(prop => {
                            const PropertyImage = prop.image;
                            return (
                                <RepositoryPropertyElement key={prop.type}>
                                    <PropertyImage/>
                                    <span>{prop.value}</span>
                                </RepositoryPropertyElement>
                            )
                        })}
                    </RepositoryPropertyList>
                </RepositoryFooter>
            </TileContent>
        </Tile>
    )
}

export default Repository;