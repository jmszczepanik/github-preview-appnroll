import React from 'react';
import Repository from './Repository';
import AppContext from '../AppContext';
import { IGithubRepository } from './RepositoryType';
import { TileList } from './Repository.styled';


const RepositoryList: React.FC = () => {
    const {repos: {listState: {list}, searchPhrase, likeRepo, liked}, languages: {selected}} = React.useContext(AppContext);

    const getFilteredRepos = (list: IGithubRepository[], phrase: string, language: string) =>
        list.filter(repo => (!language || repo.language === language) && repo.name.includes(phrase));

    const filteredRepos = getFilteredRepos(list, searchPhrase, selected);

    return (
        <TileList>
            {filteredRepos.map(repo =>
                <Repository
                    key={repo.id}
                    repo={repo}
                    isLiked={liked.includes(repo.id)}
                    onLike={likeRepo}
                />
            )}
        </TileList>
    )

}

export default RepositoryList;