import styled from 'styled-components';
import { mainBorder, titleDarkGrey, buttonMain, LightText, Button, lightGrayBackground, TitleText } from '../styles/CommonStyles.styled';

interface IRepositoryColor {
    color?: string;
}

export const TileList = styled.div`
    display: flex;
    flex-wrap: wrap;
`;

export const Tile = styled.div`
    width: 375px;
    height: 350px;
    border-radius: 5px;
    border: 1px solid ${mainBorder};
    overflow: hidden;
    margin-top: 25px;

    &:not(:nth-child(3n + 1)) {
        margin-left: 15px;
    }
`;

export const TileContent = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 100%;
`;

export const LanguageColorHeader = styled.div<IRepositoryColor>`
    height: 6px;
    background-color: ${props => props.color};
`;

export const RepositoryContent = styled.div`
    padding: 32px;
`;

export const RepositoryHeader = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
`;

export const RepositoryName = styled(TitleText)`
    font-size: 24px;
    flex: 1;
`;

export const RepositoryButton = styled(Button)`
    width: 32px;
    height: 32px;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 0;
`;

export const RepositoryLink = styled.a`
    margin-top: 16px;
    cursor: pointer;
    text-decoration: none;
    display: flex;
    align-items: center;
    color: ${buttonMain};

    &:hover {
        text-decoration: underline;
    }

    span {
        margin-left: 10px;
    }
`;

export const RepositoryDescription = styled(LightText)`
    font-size: 16px;
`;

export const RepositoryFooter = styled.div`
    height: 86px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 35px 32px;
    background-color: ${lightGrayBackground}
`;

export const RepositoryPropertyList = styled.div`
    display: flex;
`;

export const RepositoryPropertyElement = styled.div<IRepositoryColor>`
    display: flex;
    align-items: center;
    color: ${titleDarkGrey};
    font-size: 12px;

    &:not(:first-child) {
        margin-left: 28px;
    }

    span {
        margin-left: 8px;
    }
    
    ${({color}) => color && `
        circle {
            fill: ${color};
        }
    `}
`;

export const RepositoryLanguage = styled.div`
    width: 65px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    margin-left: 4px;
`;