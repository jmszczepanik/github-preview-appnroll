import React, { useState, useEffect, useRef } from 'react';
import { Select, Option, OptionList, ActiveOption } from './Select.styled';

interface ISelectProps {
    value: string;
    onChange: (option: string) => void;
    defaultValue: string;
    options: string[];
}

const SelectInput: React.FC<ISelectProps> = ({options, defaultValue, onChange, value}) => {
    const [isActive, updateState] = useState<boolean>(false);
    const selectRef = useRef<HTMLDivElement>(null);

    const handleClickOutside = (e: MouseEvent) => {
        if(selectRef.current && !selectRef.current.contains(e.target as Node)) {
            updateState(false);
        }
    }

    const handleSelectClick = () => updateState(!isActive);

    useEffect(() => {
        document.addEventListener('mousedown', handleClickOutside);
        
        return () => {
            document.removeEventListener('mousedown', handleClickOutside)
        }
    }, [])
    

    return (
        <Select
            ref={selectRef}
            onClick={handleSelectClick}
        >
            <ActiveOption isDisabled={!value}>{value ? value : defaultValue}</ActiveOption>
            {isActive && (
            <OptionList>
                {options.map(opt => (
                        <Option
                            key={opt}
                            onClick={() => onChange(opt)}
                        >
                        {opt}
                        </Option>
                    ))}
            </OptionList>
            )}
        </Select>
    )
}

export default SelectInput;