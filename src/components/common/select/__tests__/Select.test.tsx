import React from 'react';
import SelectInput from '../SelectInput';
import { shallow } from 'enzyme';
import { Select, ActiveOption, Option } from '../Select.styled';

describe('Select input', () => {
    it('render select input', () => {
        const wrapper = shallow(
        <SelectInput
            value={''}
            onChange={jest.fn()}
            defaultValue={''}
            options={[]}
        />);

        expect(wrapper).toMatchSnapshot();
        
    })

    it('render select input with options', () => {
        const wrapper = shallow(
            <SelectInput
                value={''}
                onChange={jest.fn()}
                defaultValue={''}
                options={['opt1', 'opt2', 'opt3']}
            />);
        
        wrapper.find(Select).simulate('click');

        expect(wrapper).toMatchSnapshot();
    })

    it('render default value if no value provided', () => {
        const defaultVal = 'defeaultValue';
        const wrapper = shallow(
            <SelectInput
                value={''}
                onChange={jest.fn()}
                defaultValue={defaultVal}
                options={[]}
            />);
        
        expect(wrapper.find(ActiveOption).text()).toEqual(defaultVal);
    })

    it('render correct amount of options', () => {
        const options = ['opt1', 'opt2', 'opt3'];
        const wrapper = shallow(
            <SelectInput
                value={''}
                onChange={jest.fn()}
                defaultValue={''}
                options={options}
            />);
        
        wrapper.find(Select).simulate('click');

        expect(wrapper.find(Option)).toHaveLength(options.length);
    })
})