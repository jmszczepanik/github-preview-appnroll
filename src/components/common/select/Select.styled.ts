import styled from 'styled-components';
import { baseInput, inputBackground, inputBorder, textInverse, inputBackgroundInverse, inputPlaceholder } from '../../styles/CommonStyles.styled';

interface IOptionProps {
    isDisabled?: boolean;
}

export const Select = styled.div`
    ${baseInput}
    width: 275px;
    position: relative;
`;

export const OptionList = styled.div`   
    top: 40px;
    right: 0;
    left: 0;
    position: absolute;
    overflow: scroll;
    max-height: 300px;
    border: 1px solid ${inputBorder};
`;


const BaseOption = styled.div<IOptionProps>`
    display: flex;
    align-items: center;
    height: 38px;
    font-size: 16px;
    padding: 0 5px;
    cursor: pointer;
    background-color: ${inputBackground};
`

export const Option = styled(BaseOption)`
    

    &:not(:last-child) {
        border-bottom: 1px solid ${inputBorder};
    }

    &:hover {
        background-color: ${inputBackgroundInverse};
        color: ${textInverse}
    }
`;

export const ActiveOption = styled(BaseOption)`
    ${({isDisabled}) => isDisabled && `
        color: ${inputPlaceholder};
    `}
`;