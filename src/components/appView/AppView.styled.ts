import styled from 'styled-components';

export const ErrorText = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 26px;
    font-family: Quicksand-Medium;
`;