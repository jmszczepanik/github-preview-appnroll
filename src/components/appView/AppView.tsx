import React, { useContext } from 'react';
import AppContext from '../AppContext';
import SearchBar from '../searchBar/SearchBar';
import Header from '../header/Header';
import RepositoryList from '../repository/RepositoryList';
import { ErrorText } from './AppView.styled';
import Loader from '../loader/Loader';

const HomeView = () => (
    <React.Fragment>
        <Header/>
        <SearchBar/>
        <RepositoryList/>
    </React.Fragment>
)

const ErrorView = () => (
    <ErrorText>Sorry, something went wrong. Please try again later</ErrorText>
)

const LoadingView = () => <Loader/>

const AppView = () => {
    const ctx = useContext(AppContext);
    const { isLoading } = ctx.repos.listState;
    
    if(isLoading) {
        return <LoadingView/>;
    }

    if(ctx.isError) {
        return <ErrorView/>
    }

    return <HomeView/>
}

export default AppView;