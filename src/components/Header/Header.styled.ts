import styled from 'styled-components';
import { LightText, logoBackground, mainBorder, titleDarkGrey, TitleText } from '../styles/CommonStyles.styled';

const FlexWrapper = styled.div`
    display: flex;
`;

export const HeaderWrapper = styled(FlexWrapper)``;
export const HeaderLogo = styled(FlexWrapper)`
    justify-content: center;
    align-items: center;
    height: 100px;
    width: 100px;
    background-color: ${logoBackground};
    border: 1px solid ${mainBorder};
    border-radius: 5px;
    padding: 7px 0;
`;
export const HeaderDetails = styled(FlexWrapper)`
    justify-content: center;
    flex-direction: column;
    margin-left: 20px;
`;

export const DetailsName = styled(TitleText)`
    font-size: 20px;
`

export const DetailsSubtext = styled(LightText)`
    font-size: 14px;
    margin-top: 8px;
`;

export const DetailsContact = styled(FlexWrapper)`
    margin-top: 8px;
`;

export const ContactElement = styled(FlexWrapper)`
    align-items: center;
    color: ${titleDarkGrey};
    font-size: 14px;

    &:not(:first-child) {
        margin-left: 24px;
    }

    svg {
        margin-right: 4px;
    }

    a {
        text-decoration: none;
        color: ${titleDarkGrey};
    }
`