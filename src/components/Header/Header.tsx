import React from 'react';
import Logo from '../../assets/logo.svg';
import Location from '../../assets/location.svg';
import Link from '../../assets/link.svg';
import {
    HeaderWrapper,
    DetailsContact,
    HeaderLogo,
    HeaderDetails,
    DetailsSubtext,
    DetailsName,
    ContactElement
 } from './Header.styled';


const Header: React.FC = () => {
    return (
        <HeaderWrapper>
            <HeaderLogo>
                <Logo/>
            </HeaderLogo>
            <HeaderDetails>
                <DetailsName>
                    App'n'roll repositories
                </DetailsName>
                <DetailsSubtext>
                    We rock IT!
                </DetailsSubtext>
                <DetailsContact>
                    <ContactElement>
                        <Location/>
                        Warsaw, PL
                    </ContactElement>
                    <ContactElement>
                        <Link/>
                        <a href="http://appnroll.com">
                            http://appnroll.com
                        </a>
                    </ContactElement>
                </DetailsContact>
            </HeaderDetails>
        </HeaderWrapper>
    )
}

export default Header;