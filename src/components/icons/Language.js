import React from "react";

const SvgLanguage = props => (
  <svg width={16} height={16} {...props}>
    <circle cx={8} cy={8} r={8} fill="#553E7A" fillRule="evenodd" />
  </svg>
);

export default SvgLanguage;
