import React, { useEffect, useState } from 'react';
import AppContext, { initialRepoState } from './AppContext';
import { fetchLikedRepos, likeRepoById } from './storage/StorageRepos';
import { IGithubRepository, IRepositoryListState } from './repository/RepositoryType';
import { AppWrapper } from './App.styled';
import AppView from './appView/AppView';

const getUniqLanguages = (repos: IGithubRepository[]) => 
Array.from(new Set(repos.map(repo => repo.language)));

const App: React.FC = () => {

  const [state, setState] = useState<IRepositoryListState>(initialRepoState);
  const [isError, setError] = useState<boolean>(false);
  const [searchPhrase, updatePhrase] = useState<string>('');
  const [selectedLanguage, selectLanguage] = useState<string>('');
  const [liked, updateLiked] = useState<number[]>([]);
  
  useEffect(() => {
    fetchLikedRepos(updateLiked);
    fetch('https://api.github.com/users/appnroll/repos')
    .then(res => {
      if(res.ok) {
        return res.json();
      } else {
        throw new Error(`Fetching interrupted. Status code: ${res.status}`);
      }
    })
    .then(res => setState({
      list: res,
      isLoading: false
    }))
    .catch(_err => setError(true));
  }, [])

  const context = {
    repos: {
      listState: state,
      searchPhrase,
      liked,
      onSearch: updatePhrase,
      likeRepo: likeRepoById(liked, updateLiked)
    },
    languages: {
      list: getUniqLanguages(state.list),
      selected: selectedLanguage,
      onSelect: selectLanguage,
    },
    isError
  }

  return (
    <AppWrapper>
      <AppContext.Provider value={context}>
        <AppView/>
      </AppContext.Provider>
    </AppWrapper>    
  );
}

export default App;