import styled from 'styled-components';

export const lightGrayBackground = 'rgba(39,124,220,0.04)';
export const logoBackground = '#0F1436';
export const mainBorder = '#E3E5E8';

export const lightTextGrey = '#7D8CA1'
export const titleDarkGrey = '#243C56';
export const textInverse = '#FFF';

export const inputBorder = '#C8D2E7';
export const inputBackground = '#FFF';
export const inputBackgroundInverse = '#0F1436';
export const buttonMain = '#0062FF';

export const inputPlaceholder = '#818FA3';

export const baseInput = `
    height: 40px;
    border-radius: 2px;
    box-shadow: 0 4px 10px 0 rgba(189,199,222,0.23);
    border-radius: 2px;
    font-size: 16px;
    border: 1px solid ${inputBorder};
    padding: 0 5px;
    background-color: ${inputBackground};

    &:focus,
    &:active {
        outline: 0;
    }
`;

export const LightText = styled.div`
    color: ${lightTextGrey};
    font-weight: 300;
`;

export const TitleText = styled.div`
    color: ${titleDarkGrey};
    font-family: 'Quicksand-Medium', sans-serif;
`;

export const TextInput = styled.input`
    ${baseInput}
    width: 190px;

    &::placeholder {
        color: ${inputPlaceholder};
    }
`;

export const Button = styled.button`
    background-color: ${buttonMain};
    font-size: 16px;
    color: ${textInverse};
    height: 40px;
    padding: 10px 14px;
    border: 0;
    cursor: pointer;
    transition: background-color .1s ease;

    &:focus {
        outline: 0;
    }

    &:active {
        background-color: ${inputBackgroundInverse};
    }
`;