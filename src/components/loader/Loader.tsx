import React from 'react';
import { LoaderAnimation } from './Loader.styled';

const Loader = () => <LoaderAnimation/>

export default Loader;